/**
 * Created by paul on 07/01/15.
 */
describe('greeting form', function(){
    browser.ignoreSynchronization = true; //not an AngularApp
    
    it('says Hello', function () {
        var hello = require('../pages/hello.js');
        browser.get('http://localhost/hello.php');
        
        hello.fillInForm('Paul');
        
        var msg = hello.checkMessage();
        expect(msg).toEqual('Hello Paul');
    });

});