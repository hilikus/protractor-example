/**
 * Created by paul on 07/01/15.
 */
var Hello = function(){
    this.nameInput = $("#greeting input[name=name]");
    this.submitButton = $('#submit');
    this.message = $('h1');

    this.fillInForm = function(name){
        this.nameInput.sendKeys(name);
        this.submitButton.click();
    };
    
    this.checkMessage = function(){
    	return this.message.getText();
    };

};

module.exports = new Hello();