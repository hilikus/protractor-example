# README #



### What is this repository for? ###

Automated website testing - to quickly launch a browser and run typical user interactions and check they're expected outcomes.


### How do I get set up? ###

In order to run these tests you'll need a few things installed first:

* node.js
* Protractor "npm install -g protractor"
* Update webdriver (it came with protractor) "webdriver-manager update"
* Run a local webdriver in a terminal "webdriver-manager start"

### Files explained ###
* conf.js - your configuration file, this includes the url of your webdriver and the location of your tests
* hello.php - our example page with a basic form element
* pages/hello.js - our page object, this defines the elements on the page the test can interact with
* tests/greeting-spec.js - our test, a simple definition for our test and our expected outcome. Contains the url of the page we're testing (hello.php)

### Running tests ###
To run this test simply hand protractor the conf file:
"protractor conf.js"

### More info ###
official project page: http://angular.github.io/protractor/#/